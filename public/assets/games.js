const playerOptions = document.querySelectorAll(".player");
const comOptions = document.querySelectorAll(".com");
const resultBanner = document.querySelector("#result p");
const bgResultBanner = document.getElementById("result");
const refreshBtn = document.getElementById("refresh");

class Game {
  constructor(playerOne, playerTwo) {
    this.playerOne = playerOne;
    this.playerTwo = playerTwo;
    this.result = 0;
  }

  start(key) {
    if (!game.getResult()) {
      com.randChoose();
      player.choosing(key);

      playerOptions[key].classList.add("active");

      //hapus class player agar tidak ada efek hover
      for (let i = 0; i < playerOptions.length; i++) {
        playerOptions[i].classList.remove("player");
      }

      //jalankan suit
      game.suit();

      //Tampilakan Log
      console.log(game.suit());
    } else {
      alert("Refresh dulu game nya");
    }
  }

  suit() {
    const playerOneName = this.playerOne.getName();
    const playerTwoName = this.playerTwo.getName();

    const playerOneChoosen = this.playerOne.getChoosen();
    const playerTwoChoosen = this.playerTwo.getChoosen();

    //set result ke 1 agar tidak bisa dimainkan sebelum direfresh
    this.result = 1;

    //logika games
    if (playerOneChoosen === playerTwoChoosen) {
      this.showResult("DRAW");
      return "DRAW";
    }

    if (playerOneChoosen === "batu" && playerTwoChoosen === "gunting") {
      this.showResult(`${playerOneName} WIN`);
      return `${playerOneName} WIN`;
    }

    if (playerOneChoosen === "kertas" && playerTwoChoosen === "batu") {
      this.showResult(`${playerOneName} WIN`);
      return `${playerOneName} WIN`;
    }

    if (playerOneChoosen === "gunting" && playerTwoChoosen === "kertas") {
      this.showResult(`${playerOneName} WIN`);
      return `${playerOneName} WIN`;
    }

    this.showResult(`${playerTwoName} WIN`);
    return `${playerTwoName} WIN`;
  }

  getResult() {
    return this.result;
  }

  showResult(result) {
    //ganti background sesuai hasil
    if (result === "DRAW") {
      bgResultBanner.style.backgroundColor = "#035B0C";
    } else {
      bgResultBanner.style.backgroundColor = "#4C9654";
    }

    //rotate background
    bgResultBanner.classList.add("transform-result");

    //ganti text sesuai hasil
    resultBanner.innerHTML = result;

    //ganti warna text hasil
    resultBanner.classList.add("text-white");
  }

  refresh() {
    //ganti background kembali ke transparent
    bgResultBanner.style.backgroundColor = "rgba(0,0,0,0.0)";

    //hapus rotate background
    bgResultBanner.classList.remove("transform-result");

    //ganti text ke VS
    resultBanner.innerHTML = "VS";

    //ganti warna text hasil ke merah
    resultBanner.classList.remove("text-white");

    for (let i = 0; i < playerOptions.length; i++) {
      playerOptions[i].classList.remove("active");
    }

    for (let i = 0; i < comOptions.length; i++) {
      comOptions[i].classList.remove("active");
    }

    //hapus class player agar tidak ada efek hover
    for (let i = 0; i < playerOptions.length; i++) {
      playerOptions[i].classList.add("player");
    }

    //kembalikan value result ke 0 agar bisa dimainkan
    this.result = 0;
  }
}

class Player {
  constructor(name) {
    this.name = name;
    this.option = ["batu", "kertas", "gunting"];
    this.choosen = "";
  }

  choosing(chooseByPlayer) {
    this.choosen = this.option[chooseByPlayer];
  }

  randChoose() {
    const rand = Math.floor(Math.random() * 3);
    comOptions[rand].classList.add("active");
    this.choosen = this.option[rand];
  }

  getName() {
    return this.name;
  }

  getChoosen() {
    return this.choosen;
  }
}

//init
let player = new Player("PLAYER");
let com = new Player("COM");
let game = new Game(player, com);

//listener tombol player
playerOptions.forEach((option, key) => {
  option.addEventListener("click", function () {
    game.start(key);
  });
});

//listener tombol refresh
refreshBtn.addEventListener("click", function () {
  game.refresh();
});
