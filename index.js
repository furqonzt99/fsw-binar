const express = require("express");
const app = express();
const morgan = require("morgan");
const session = require("express-session");
const layouts = require("express-ejs-layouts")

const apiRoute = require("./routes/apiRoute");

const gameRoute = require("./routes/gameRoute");
const authRoute = require("./routes/authRoute");
const userRoute = require("./routes/userRoute");

const PORT = 8001;

//gunakan session
app.use(
  session({
    secret: 'Jangan bilang bilang',
    cookie: {
      maxAge: 60000
    }
  }
));

//handle static file asset
app.use(express.static("public"));

//gunakan view engine
app.set("view engine", "ejs");

//gunakan express-ejs-layout
app.use(layouts);

//gunakan morgan
app.use(morgan("dev"));

//parser ulrencode
app.use(express.urlencoded({ extended: false }));

//parser json
app.use(express.json());

//middleware
app.use(function logger(req, res, next) {
  morgan(":method :url :status :res[content-length] - :response-time ms");
  next();
});

app.get("/", (req, res) => {
  res.render("index", {
    layout: "layouts/guest-layout",
    title: "Home - Traditional Game",
  });
});

app.use("/", apiRoute);

app.use("/", authRoute);
app.use("/", gameRoute);
app.use("/", userRoute);


//handle error 5xx
app.use(function (err, req, res, next) {
  if (err) {
    res.status(500).json({
      status: res.status,
      message: err.message || "Internal Server Error",
    });
  }
});

//handle error 4xx
app.use(function (req, res, next) {
  res.status(404).json({
    status: res.status,
    message: "Not Found",
  });
});

app.listen(PORT, () => {
  console.log(`Listening on port : ${PORT}`);
});
