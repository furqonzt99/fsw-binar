const { User, BiodataUser, UserGameHistory } = require("../models");

const apiUserController = {
  async create(req, res) {
    const { username, password, name, email, bornDate } = req.body;
    const data = await User.create(
      {
        username: username,
        password: password,
        BiodataUser: {
          name: name,
          email: email,
          bornDate: bornDate,
        },
      },
      {
        include: BiodataUser,
      }
    );

    return res.json(data);
  },
  async read(req, res) {
    let data;

    if (!req.query.id) {
      data = await User.findAll({
        attributes: {
          exclude: ["password"],
        },
        include: [
          {
            model: BiodataUser,
          },
          {
            model: UserGameHistory,
          },
        ],
      });
    } else {
      data = await User.findOne({
        where: {
          id: req.query.id,
        },
        attributes: {
          exclude: ["password"],
        },
        include: [
          {
            model: BiodataUser,
          },
        ],
      });
    }

    return res.json(data);
  },
  async update(req, res) {
    const { id, name, email, bornDate } = req.body;

    const data = await BiodataUser.update(
      {
        name: name,
        email: email,
        bornDate: bornDate,
      },
      {
        where: {
          userId: id,
        },
      }
    );

    return res.json(data);
  },
  async delete(req, res) {
    const { id } = req.body;

    console.log(req.body);

    const data = await User.destroy({
      where: {
        id: id,
      },
    });

    return res.json(data);
  },
};

module.exports = {
  apiUserController,
};
