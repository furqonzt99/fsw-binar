const gameController = {
  play(req, res) {
    res.render("games", {
      layout: 'layouts/games-layout',
      title: 'Play Suit Game'
    });
  },
};

module.exports = gameController;
