const axios = require("axios");

const userController = {
  async index(req, res) {
    try {
      const response = await axios.get("http://localhost:8001/api/v1/users");

      res.render("user/index", {
        layout: "layouts/dashboard-layout",
        title: "Manajemen User",
        data: response.data,
      });
    } catch (error) {
      console.log(error);
    }
  },
  create(req, res) {
    res.render("user/create", {
      layout: "layouts/dashboard-layout",
      title: "Create User Data",
    });
  },
  async edit(req, res) {
    try {
      const response = await axios.get(
        `http://localhost:8001/api/v1/users?id=${req.query.id}`
      );

      if (!response.data) {
        res.render("error", {
          layout: "layouts/error-layout",
          title: "404 - Not Found",
        });
      }

      res.render("user/edit", {
        layout: "layouts/dashboard-layout",
        title: "Edit User",
        data: response.data,
      });
    } catch (error) {
      console.log(error);
    }
  },
};

module.exports = userController;
