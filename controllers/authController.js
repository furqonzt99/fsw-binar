const users = require("../data/users.json");

const authController = {
  login(req, res) {
    res.render("auth/login", {
      layout: "layouts/guest-layout",
      title: "Login - Traditional Games"
    });
  },
  loginProcess(req, res) {
    const { username, password } = req.body;

    console.log(username, password);

    const findUser = users.find(
      (item) => item.username === username && item.password === password
    );

    if (!findUser) {
      throw new Error("Username dan password salah");
    }

    delete findUser.password;

    req.session.isLoggedIn = true;

    return res.redirect("/users");
  },
};

module.exports = authController;
