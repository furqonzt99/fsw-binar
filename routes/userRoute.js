const express = require("express");
const userController = require("../controllers/userController");
const router = express.Router();

router.get("/users", userController.index);
router.get("/users-create", userController.create);
router.get("/users-edit", userController.edit);

module.exports = router;
