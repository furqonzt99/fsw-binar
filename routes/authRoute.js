const express = require("express");
const authController = require("../controllers/authController");
const router = express.Router();
const authMiddleware = require("../middleware/authMiddleware");

router.get("/login", authController.login);
router.post("/login", authController.loginProcess);

module.exports = router;
