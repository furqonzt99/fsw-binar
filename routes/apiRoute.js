const express = require("express");
const { apiUserController } = require("../controllers/apiController");
const router = express.Router();

// API Router
router.get("/api/v1/users", apiUserController.read);
router.post("/api/v1/users", apiUserController.create);
router.put("/api/v1/users", apiUserController.update);
router.delete("/api/v1/users", apiUserController.delete);

module.exports = router;
